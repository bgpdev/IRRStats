package irr;

import collections.trees.BinaryTree;
import collections.trees.Node;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import network.entities.Prefix;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.util.BitSet;
import java.util.Date;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

/**
 * Routing Information Base
 * An instance of this class holds the Routing Information Base (RIB) information of a particular collector.
 * It holds all routes previously advertised.
 */
public class RoutingInformationBase
{
    /** The static HashMap that holds all routes currently in the RIB. */
    public HashMap<String, Node<RIB_AFI>> map = new HashMap<>();
    private BinaryTree<RIB_AFI> ipv4_tree = new BinaryTree<>();
    private BinaryTree<RIB_AFI> ipv6_tree = new BinaryTree<>();

    public RoutingInformationBase(File file) throws Exception
    {
        load(file);
    }

    public RoutingInformationBase(String id, String archive, Date date) throws Exception
    {
        /* -----------------------------------------
         * Retrieve the Routing Information Base
         * -----------------------------------------*/

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        // Check if the archived file is already downloaded.
        File file = new File("archives/" + format.format(new Date()) + "/rib/" + id + "-0000.gz");
        if (!file.exists())
        {
            file.getParentFile().mkdirs();
            file.createNewFile();

            if(!format.format(date).equals(format.format(new Date())))
                throw new Exception("The routing archives of this date are not available");
            else
            {
                System.out.println("Downloading " + archive + " ...");
                download(archive, file);
                System.out.println("Download complete!");
            }
        }
    }

    public void load(File file) throws Exception
    {
        System.out.println("Loading RIB ...");
        DataInputStream stream = new DataInputStream(new GZIPInputStream(new FileInputStream(file), 110000000));

        try
        {
            MRTReader reader = new MRTReader(stream);

            int counter = 0;
            while (stream.available() > 0)
            {
                MRTRecord entry = reader.read();

                if ((entry instanceof RIB_AFI))
                {
                    RIB_AFI afi = (RIB_AFI)entry;

                    if(++counter % 100000 == 0)
                        System.out.println("Counter: " + counter);

                    if(afi.prefix.prefix == null)
                        continue;

                    Node<RIB_AFI> result;
                    if(afi.prefix.type == Prefix.Type.IPv4)
                        result = ipv4_tree.insert(BitSet.valueOf(afi.prefix.prefix), afi.prefix.length, afi);
                    else
                        result = ipv6_tree.insert(BitSet.valueOf(afi.prefix.prefix), afi.prefix.length, afi);

                    map.put(afi.prefix.toString(), result);
                }
            }
        } catch (EOFException e)
        {
            System.out.println("[" + new Date().toString() + "] End of stream found!");
            stream.close();
        }
    }

    private void download(String archive, File file) throws Exception
    {
        URL website = new URL(archive);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }

    public Node<RIB_AFI> getPrefix(String prefix)
    {
        try
        {
            IPAddress address = new IPAddressString(prefix).toAddress();

            if(map.containsKey(address.toString()))
                return map.get(address.toString());

            if(address.isIPv4())
                return ipv4_tree.get(BitSet.valueOf(address.getBytes()), address.getPrefixLength());
            else
                return ipv6_tree.get(BitSet.valueOf(address.getBytes()), address.getPrefixLength());
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}