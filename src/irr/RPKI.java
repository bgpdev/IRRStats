package irr;

import collections.trees.BinaryTree;
import collections.trees.Node;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;

public class RPKI
{
    public static class ROA
    {
        public String prefix;
        public Long origin;
        public Integer maxLength;
    }

    public static BinaryTree<ArrayList<ROA>> getRPKITree(Instant time)
    {
        try
        {
            BinaryTree<ArrayList<ROA>> tree = new BinaryTree<>();

            // Open up the file to store the archive at.
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);
            File file = new File("archives/" + formatter.format(time) + "/rpki/rpki.json");

            JsonParser parser = new JsonParser();
            JsonElement root = parser.parse(new FileReader(file));
            JsonArray array = root.getAsJsonObject().get("roas").getAsJsonArray();
            for(JsonElement element : array)
            {
                JsonObject roa = element.getAsJsonObject();
                ROA r = new ROA();
                r.prefix = roa.get("prefix").getAsString();
                r.origin = Long.parseLong(roa.get("asn").getAsString().replace("AS", ""));
                r.maxLength = Integer.parseInt(roa.get("maxLength").getAsString());

                IPAddress address = new IPAddressString(r.prefix).toAddress();
                BitSet set = BitSet.valueOf(address.getBytes());
                Node<ArrayList<ROA>> node = tree.getExact(set, address.getPrefixLength());

                if (node == null || node.value == null)
                {
                    ArrayList<ROA> x = new ArrayList<>();
                    x.add(r);
                    tree.insert(set, address.getPrefixLength(), x);
                } else
                    node.value.add(r);
            }

            return tree;
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            System.exit(-1);
            return null;
        }
    }

    public static void download(Instant time) throws Exception
    {
        if(!time.equals(Instant.now().truncatedTo(ChronoUnit.DAYS)))
            throw new Exception("[RPKI] Downloading Historic versions of RPKI is not yet supported.");

        System.out.println("[" + new Date().toString() + "] Checking for RPKI Json exports..");

        DateTimeFormatter x = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneOffset.UTC);

        File file = new File("archives/" + x.format(time) + "/rpki/rpki.json");
        if(file.exists())
            return;

        try
        {
            URL url = new URL("http://localcert.ripe.net:8088/export.json");
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            if(http.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new Exception("Failed to download the RPKI Json export.");
            else
            {
                System.out.println("[" + new Date().toString() + "] Downloading RPKI Json Export ...");
                FileUtils.copyURLToFile(url, file);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();

            // Delete the file so we can try again on subsequent executions.
            file.delete();

            // Throw a new exception causing the system to reboot.
            throw new Exception("Failed to download the RPKI Json export.");
        }

        System.out.println("[" + new Date().toString() + "] Done.");
    }
}
