package irr;

import com.google.common.io.ByteStreams;
import po.POWriter;
import rpsl.RPSLObject;
import rpsl.RPSLReader;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class IRRDownloader
{
    /**
     * Downloads the IRR archives if they are not already present.
     */
    public static void download(ArrayList<RoutingRegistry> registries) throws Exception
    {
        // Retrieve the date of today.
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (RoutingRegistry registry : registries)
        {
            if(registry.archives.length == 0)
                continue;

            File file = new File("archives/" + format.format(new Date()) + "/irr/raw/" + registry.name + ".db.gz");

            // If the file already exists, skip this Routing Registry download.
            if (!file.exists())
            {
                // Create the parent folders and the file if not existing.
                file.getParentFile().mkdirs();
                file.createNewFile();

                for (String archive : registry.archives)
                {
                    System.out.println("Downloading: " + archive);

                    /* ----------------------------------------
                     * Download the databases and store them.
                     * ----------------------------------------*/
                    try
                    {
                        URL url = new URL(archive);
                        URLConnection connection = url.openConnection();

                        OutputStream output;
                        if (archive.endsWith(".gz"))
                            output = new FileOutputStream(file, true);
                        else
                            output = new GZIPOutputStream(new FileOutputStream(file, true));

                        ByteStreams.copy(connection.getInputStream(), output);
                        output.close();
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();

                        // Delete the file so we can try again on subsequent executions.
                        file.delete();

                        // Throw a new exception causing the system to reboot.
                        throw new Exception("Failed to download the latest IRR dump.");
                    }
                }
            }

            /* ----------------------------------------
             * Parse the database files to PO format.
             * ----------------------------------------*/
            File po = new File("archives/" + format.format(new Date()) + "/irr/po/" + registry.name + ".po");

            if(!po.exists())
            {
                System.out.println("Generating PO for: " + registry.name);
                RPSLReader reader = new RPSLReader(new GZIPInputStream(new FileInputStream(file)));

                HashMap<String, Set<Long>> prefixes = new HashMap<>();
                po.getParentFile().mkdirs();

                POWriter writer = new POWriter(new DataOutputStream(new FileOutputStream(po, true)));

                reader.STRICT_PARSING = false;
                RPSLObject object = reader.read();
                while (object != null)
                {
                    if (object.hasProperty("route") && object.hasProperty("origin") &&
                            object.getProperty("route").contains("/") &&
                            object.getProperty("origin").toUpperCase().replaceAll("AS", "").matches("[0-9]+"))
                    {
                        if(!prefixes.containsKey(object.getProperty("route")))
                            prefixes.put(object.getProperty("route"), new HashSet<>());

                        prefixes.get(object.getProperty("route")).add(Long.parseLong(object.getProperty("origin").toUpperCase().replaceAll("AS", "")));

                    } else if (object.hasProperty("route6") && object.hasProperty("origin") &&
                            object.getProperty("route6").contains("/") &&
                            object.getProperty("origin").toUpperCase().replaceAll("AS", "").matches("[0-9]+"))
                    {
                        if(!prefixes.containsKey(object.getProperty("route6")))
                            prefixes.put(object.getProperty("route6"), new HashSet<>());

                        prefixes.get(object.getProperty("route6")).add(Long.parseLong(object.getProperty("origin").toUpperCase().replaceAll("AS", "")));

                    }

                    object = reader.read();
                }

                for(Map.Entry<String, Set<Long>> x : prefixes.entrySet())
                {
                    String[] split = x.getKey().split("/");
                    if (split[1].matches("\\d+"))
                        for(Long i : x.getValue())
                            writer.write(split[0], Integer.parseInt(split[1]), i);
                }

                reader.close();
            }
        }
    }
}
