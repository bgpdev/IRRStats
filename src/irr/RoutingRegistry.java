package irr;

import collections.trees.BinaryTree;
import collections.trees.Node;
import com.google.gson.JsonObject;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import po.PO;
import po.POReader;
import utility.Matcher;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class RoutingRegistry
{
    public final String name;

    /** List of RPSL archives. */
    public final String[] archives;

    /** List of Routes */
    public final HashMap<String, Entry> routes = new HashMap<>();

    /**
     *
     */
    public class Entry
    {
        public final PO po;
        public Matcher.Classification classification = Matcher.Classification.NONE;

        public Entry(PO po)
        {
            this.po = po;
        }
    }

    /**
     * Constructor
     * @param file
     */
    public RoutingRegistry(File file) throws Exception
    {
        POReader reader = new POReader(new DataInputStream(new FileInputStream(file)));

        PO object = reader.read();
        while (object != null)
        {
            routes.put(object.prefix, new Entry(object));
            object = reader.read();
        }


        this.name = file.getName().split("\\.")[0];
        this.archives = null;
    }

    /**
     * Constructor
     * @param name
     * @param archives
     */
    public RoutingRegistry(String name, String[] archives)
    {
        this.name = name;
        this.archives = archives;
    }

    public JsonObject getStatistics()
    {
        HashMap<String, Long> count = new HashMap<>();

        for(Map.Entry<String, Entry> x : routes.entrySet())
        {
            String classification = x.getValue().classification.toString();
            if(!count.containsKey(classification))
                count.put(classification, 0L);

            count.put(classification, count.get(classification) + 1);
        }

        JsonObject report = new JsonObject();
        report.addProperty("ROUTES", routes.size());
        for(Map.Entry<String, Long> x : count.entrySet())
            report.addProperty(x.getKey(), x.getValue());

        return report;
    }

    public static BinaryTree<ArrayList<PO>> getRegistryTree(Date date) throws IOException
    {
        BinaryTree<ArrayList<PO>> tree = new BinaryTree<>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try (Stream<Path> paths = Files.walk(Paths.get("archives/" + format.format(date) + "/irr/po/")))
        {
            paths.filter(Files::isRegularFile).forEach((Path path) ->
            {
                try
                {
                    System.out.println("[" + new Date().toString() + "] Loading " + path.toAbsolutePath().toString());
                    DataInputStream stream = new DataInputStream(new FileInputStream(path.toFile()));
                    POReader reader = new POReader(stream);

                    PO object = reader.read();
                    while (object != null)
                    {
                        IPAddress address = new IPAddressString(object.prefix).toAddress();
                        BitSet set = BitSet.valueOf(address.getBytes());
                        Node<ArrayList<PO>> node = tree.getExact(set, address.getPrefixLength());

                        if (node == null || node.value == null)
                        {
                            ArrayList<PO> x = new ArrayList<>();
                            x.add(object);
                            tree.insert(set, address.getPrefixLength(), x);
                        } else
                            node.value.add(object);

                        object = reader.read();
                    }

                    stream.close();
                    System.out.println("[" + new Date().toString() + "] Done!");
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            });
        }

        return tree;
    }
}
