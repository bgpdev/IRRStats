package utility;

import bgp.PathAttribute;
import bgp.utility.BGPUtility;
import collections.trees.BinaryTree;
import collections.trees.Node;
import core.Network;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import irr.RoutingInformationBase;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import po.PO;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.*;

import static utility.Matcher.Classification.*;

public class Matcher
{
    public enum Classification
    {
        DIRECT_MATCH,
        UPSTREAM_MATCH,
        LINK_MATCH,

        EXACT_DIRECT_MATCH,
        EXACT_UPSTREAM_MATCH,
        EXACT_LINK_MATCH,

        GENERIC_DIRECT_MATCH,
        GENERIC_UPSTREAM_MATCH,
        GENERIC_LINK_MATCH,

        SPECIFIC_DIRECT_MATCH,
        SPECIFIC_UPSTREAM_MATCH,
        SPECIFIC_LINK_MATCH,
        SPECIFIC_MIXED_MATCH,

        NONE
    }

    /**
     *
     * @param entry
     * @param registries
     * @param network
     * @return
     */
    public static Classification match(String prefix, RIB_AFI.RIB_ENTRY entry, BinaryTree<ArrayList<PO>> registries, Network network)
    {
        try
        {
            // Check for exact matches.
            IPAddress address = new IPAddressString(prefix).toAddress();
            BitSet set = BitSet.valueOf(address.getBytes());
            Node<ArrayList<PO>> node = registries.get(set, address.getPrefixLength());

            if (node.value != null && node.value.get(0).prefix.equals(prefix))
            {
                switch (match(entry, node.value, network))
                {
                    case DIRECT_MATCH:
                        return Classification.EXACT_DIRECT_MATCH;
                    case UPSTREAM_MATCH:
                        return Classification.EXACT_UPSTREAM_MATCH;
                    case LINK_MATCH:
                        return Classification.EXACT_LINK_MATCH;
                }
            }

            // Check for more general matches.
            {
                Node<ArrayList<PO>> x = node;
                while (x != null)
                {
                    if (x.value != null)
                    {
                        switch (match(entry, x.value, network))
                        {
                            case DIRECT_MATCH:
                                return Classification.GENERIC_DIRECT_MATCH;
                            case UPSTREAM_MATCH:
                                return Classification.GENERIC_UPSTREAM_MATCH;
                            case LINK_MATCH:
                                return Classification.GENERIC_LINK_MATCH;
                        }
                    }

                    x = x.parent;
                }
            }

            // Check for more specific matches.
            {
                int direct = 0;
                int upstream = 0;
                int link = 0;

                Queue<Node<ArrayList<PO>>> left = new LinkedList<>();
                left.add(node);

                while (!left.isEmpty())
                {
                    Node<ArrayList<PO>> x = left.remove();

                    if (x.one != null)
                        left.add(x.one);

                    if (x.zero != null)
                        left.add(x.zero);

                    if (x.value != null)
                    {
                        switch (match(entry, x.value, network))
                        {
                            case DIRECT_MATCH:
                                direct++;
                                break;
                            case UPSTREAM_MATCH:
                                upstream++;
                                break;
                            case LINK_MATCH:
                                link++;
                                break;
                            case NONE:
                                return NONE;
                        }
                    }
                }

                if (direct > 0 && upstream == 0 && link == 0)
                    return SPECIFIC_DIRECT_MATCH;
                if (direct == 0 && upstream > 0 && link == 0)
                    return SPECIFIC_UPSTREAM_MATCH;
                if (direct == 0 && upstream == 0 && link > 0)
                    return SPECIFIC_LINK_MATCH;
                if (!(direct == 0 && upstream == 0 && link == 0))
                    return SPECIFIC_MIXED_MATCH;
                else
                    return NONE;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return NONE;
        }
    }

    public static Classification match(String prefix, Map<String, PathAttribute> attributes, BinaryTree<ArrayList<PO>> registries, Network network)
    {
        try
        {
            // Check for exact matches.
            IPAddress address = new IPAddressString(prefix).toAddress();
            BitSet set = BitSet.valueOf(address.getBytes());
            Node<ArrayList<PO>> node = registries.get(set, address.getPrefixLength());

            // Check if a route object actually exists
            boolean existant = false;

            if (node.value != null && node.value.get(0).prefix.equals(prefix))
                switch (match(attributes, node.value, network))
                {
                    case DIRECT_MATCH:
                        return Classification.EXACT_DIRECT_MATCH;
                    case UPSTREAM_MATCH:
                        return Classification.EXACT_UPSTREAM_MATCH;
                    case LINK_MATCH:
                        return Classification.EXACT_LINK_MATCH;
                }

            // Check for more general matches.
            {
                Node<ArrayList<PO>> x = node;
                while (x != null)
                {
                    if (x.value != null)
                    {
                        switch (match(attributes, x.value, network))
                        {
                            case DIRECT_MATCH:
                                return Classification.GENERIC_DIRECT_MATCH;
                            case UPSTREAM_MATCH:
                                return Classification.GENERIC_UPSTREAM_MATCH;
                            case LINK_MATCH:
                                return Classification.GENERIC_LINK_MATCH;
                        }
                    }

                    x = x.parent;
                }
            }


            // Check for more specific matches.
            {
                int direct = 0;
                int upstream = 0;
                int link = 0;

                Queue<Node<ArrayList<PO>>> left = new LinkedList<>();
                left.add(node);

                while (!left.isEmpty())
                {
                    Node<ArrayList<PO>> x = left.remove();

                    if (x.one != null)
                        left.add(x.one);

                    if (x.zero != null)
                        left.add(x.zero);

                    if (x.value != null)
                    {
                        switch (match(attributes, x.value, network))
                        {
                            case DIRECT_MATCH:
                                direct++;
                                break;
                            case UPSTREAM_MATCH:
                                upstream++;
                                break;
                            case LINK_MATCH:
                                link++;
                                break;
                            case NONE:
                                return NONE;
                        }
                    }
                }

                if (direct > 0 && upstream == 0 && link == 0)
                    return SPECIFIC_DIRECT_MATCH;
                if (direct == 0 && upstream > 0 && link == 0)
                    return SPECIFIC_UPSTREAM_MATCH;
                if (direct == 0 && upstream == 0 && link > 0)
                    return SPECIFIC_LINK_MATCH;
                if (!(direct == 0 && upstream == 0 && link == 0))
                    return SPECIFIC_MIXED_MATCH;
                else
                    return NONE;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return NONE;
        }
    }

    public static Classification match(Map<String, PathAttribute> attributes, ArrayList<PO> matches, Network network)
    {
        Classification best = NONE;

        for(PO object : matches)
        {
            switch(match(attributes, object, network))
            {
                case DIRECT_MATCH:
                    return Classification.DIRECT_MATCH;
                case LINK_MATCH:
                    best = LINK_MATCH;
                    break;
                case UPSTREAM_MATCH:
                    if(best == NONE)
                        best = UPSTREAM_MATCH;
                    break;
            }
        }

        return best;
    }

    /**
     *
     * @param entry
     * @param matches
     * @param network
     * @return
     */
    public static Classification match(RIB_AFI.RIB_ENTRY entry, ArrayList<PO> matches, Network network)
    {
        Classification best = NONE;

        for(PO object : matches)
        {
            switch(match(object, entry, network))
            {
                case DIRECT_MATCH:
                    return Classification.DIRECT_MATCH;
                case LINK_MATCH:
                    best = LINK_MATCH;
                    break;
                case UPSTREAM_MATCH:
                    if(best == NONE)
                        best = UPSTREAM_MATCH;
                    break;
            }
        }

        return best;
    }

    /**
     * Check if there is a match between a route and a RIB.
     * @param route
     * @param rib
     * @return
     */
    public static Classification match(PO route, RoutingInformationBase rib, Network network)
    {
        // Check for exact matches.
        Node<RIB_AFI> prefix = rib.getPrefix(route.prefix);

        if(prefix.value != null && prefix.value.prefix.toString().equals(route.prefix))
        {
            switch (match(route, prefix.value.entries, network))
            {
                case DIRECT_MATCH:
                    return Classification.EXACT_DIRECT_MATCH;
                case UPSTREAM_MATCH:
                    return Classification.EXACT_UPSTREAM_MATCH;
                case LINK_MATCH:
                    return Classification.EXACT_LINK_MATCH;
            }
        }

        // Check for more general matches.
        {
            Node<RIB_AFI> x = prefix;
            while(x != null)
            {
                if(x.value != null)
                {
                    switch(match(route, x.value.entries, network))
                    {
                        case DIRECT_MATCH:
                            return Classification.GENERIC_DIRECT_MATCH;
                        case UPSTREAM_MATCH:
                            return Classification.GENERIC_UPSTREAM_MATCH;
                        case LINK_MATCH:
                            return Classification.GENERIC_LINK_MATCH;
                    }
                }

                x = x.parent;
            }
        }

        // Check for more specific matches.
        {
            int direct = 0;
            int upstream = 0;
            int link = 0;

            Queue<Node<RIB_AFI>> left = new LinkedList<>();
            left.add(prefix);

            while(!left.isEmpty())
            {
                Node<RIB_AFI> x = left.remove();

                if(x.value != null)
                {
                    switch (match(route, x.value.entries, network))
                    {
                        case DIRECT_MATCH:
                            direct++;
                            break;
                        case UPSTREAM_MATCH:
                            upstream++;
                            break;
                        case LINK_MATCH:
                            link++;
                            break;
                        case NONE:
                            return NONE;
                    }
                }
                else
                {
                    if(x.one != null)
                        left.add(x.one);
                    else
                        return NONE;

                    if(x.zero != null)
                        left.add(x.zero);
                    else
                        return NONE;
                }
            }

            if(direct > 0 && upstream == 0 && link == 0)
                return SPECIFIC_DIRECT_MATCH;
            if(direct == 0 && upstream > 0 && link == 0)
                return SPECIFIC_UPSTREAM_MATCH;
            if(direct == 0 && upstream == 0 && link > 0)
                return SPECIFIC_LINK_MATCH;
            if(!(direct == 0 && upstream == 0 && link == 0))
                return SPECIFIC_MIXED_MATCH;
            else
                return NONE;
        }
    }

    /**
     * Check if there is an exact match between a set of BGPEntry objects and a route.
     * @param route The route object that should be matches against the set of entries.
     * @param entries The entries that the route object should be tested against.
     * @return The classification as a result of the matching, None if no match was found.
     */
    public static Classification match(PO route, ArrayList<RIB_AFI.RIB_ENTRY> entries, Network network)
    {
        Classification best = NONE;

        for(RIB_AFI.RIB_ENTRY entry : entries)
        {
            switch(match(route, entry, network))
            {
                case DIRECT_MATCH:
                    return Classification.DIRECT_MATCH;
                case LINK_MATCH:
                    best = LINK_MATCH;
                    break;
                case UPSTREAM_MATCH:
                    if(best == NONE)
                        best = UPSTREAM_MATCH;
                    break;
            }
        }

        return best;
    }

    /**
     * Check if a certain route matches a BGPEntry given a certain Network (used for Link matches)
     * @param route
     * @param entry
     * @param network
     * @return
     */
    public static Classification match(PO route, RIB_AFI.RIB_ENTRY entry, Network network)
    {
        try
        {
            ArrayList<Long> path = BGPUtility.getReversePath(new DataInputStream(new ByteArrayInputStream(entry.attributes)));

            if(path.size() == 0)
                System.out.println("Empty path in match()");

            if(path.get(0).equals(route.origin))
                return DIRECT_MATCH;

            for(Long x : path)
                if(x.equals(route.origin))
                    return UPSTREAM_MATCH;

            if(network.inCustomerCone(path.get(0), route.origin))
                return LINK_MATCH;

            return NONE;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return NONE;
        }
    }

    /**
     *
     */
    public static Classification match(Map<String, PathAttribute> attributes, PO route, Network network)
    {
        try
        {
            ArrayList<Long> path = BGPUtility.getReversePath(attributes);

            if(path.get(0).equals(route.origin))
                return DIRECT_MATCH;

            for(Long x : path)
                if(x.equals(route.origin))
                    return UPSTREAM_MATCH;

            if(network.inCustomerCone(path.get(0), route.origin))
                return LINK_MATCH;

            return NONE;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return NONE;
        }
    }
}
