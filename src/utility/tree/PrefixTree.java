package utility.tree;

import inet.ipaddr.IPAddress;

public class PrefixTree<A>
{
    public Leaf<A> root;

    public PrefixTree(Leaf<A> root)
    {
        this.root = root;
    }

    public Leaf<A> get(IPAddress address)
    {
        return root.get(address);
    }
}
