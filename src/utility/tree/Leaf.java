package utility.tree;

import inet.ipaddr.IPAddress;

import java.util.ArrayList;

public class Leaf<A>
{
    public Leaf<A> parent;
    public final IPAddress prefix;
    public final ArrayList<Leaf<A>> children = new ArrayList<>();
    public final A value;

    public Leaf(Leaf<A> parent, IPAddress prefix, A value)
    {
        this.parent = parent;
        this.prefix = prefix;
        this.value = value;
    }

    public Leaf<A> get(IPAddress address)
    {
        // If the address being searched for is equal to this address return this.
        if(prefix.equals(address))
            return this;

        for(Leaf<A> p : children)
        {
            if(p.prefix.equals(address))
                return p;

            if(prefix.isIPv4())
            {
                byte[] x = p.prefix.getBytes();
                byte[] y = address.getBytes();
                if(x[0] == y[0] && x[1] == y[1])
                {
                    if (p.prefix.getPrefixLength() < address.getPrefixLength() && p.prefix.contains(address))
                        return p.get(address);
                }
            }

            if(prefix.isIPv6())
            {
                byte[] x = p.prefix.getBytes();
                byte[] y = address.getBytes();
                if(x[0] == y[0] && x[1] == y[1] && x[2] == y[2])
                    if (p.prefix.getPrefixLength() < address.getPrefixLength() && p.prefix.contains(address))
                        return p.get(address);
            }
        }
        return this;
    }
}