package analyzers;

import collections.trees.BinaryTree;
import com.google.gson.JsonObject;
import core.Network;
import irr.RoutingRegistry;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import po.PO;
import utility.Matcher;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class RIBAnalyzer
{
    private static BinaryTree<ArrayList<PO>> tree = new BinaryTree<>();

    /**
     * The main entry point for the RIB analyzer
     * @param argv The commandline parameters, currently unused.
     */
    public static void main(String[] argv) throws Exception
    {
        /* --------------------------------------------------
         * Load each PO file and construct a PrefixTree.
         * --------------------------------------------------*/
        Date date = new SimpleDateFormat("yyyyMMdd").parse("20180901");
        tree = RoutingRegistry.getRegistryTree(date);
        Network network = new Network(Instant.parse("2018-09-01T00:00:00Z"));

        int counter = 0;
        HashMap<String, Long> count = new HashMap<>();

        for (File file : new File("archives/2018-09-01/ribs/").listFiles())
        {
            // For each crawler
            if (!file.isDirectory())
            {
                System.out.println("[" + new Date().toString() + "] Loading " + file.getAbsolutePath());
                List<RIB_AFI> prefixes = getRIB(file);
                System.out.println("[" + new Date().toString() + "] Done!");

                /* ----------------------------------
                 * Loop over each prefix in the RIB.
                 * ----------------------------------*/

                System.out.println("[" + new Date().toString() + "] Comparing prefixes...");
                for (RIB_AFI prefix : prefixes)
                {
                    for (RIB_AFI.RIB_ENTRY x : prefix.entries)
                    {
//                        if(prefix.prefix.toString().contains(":"))
//                            continue;

                        counter++;
//                        if (counter++ % 100000 == 0)
//                            System.out.println("Counter: " + counter);

                        String classification = Matcher.match(prefix.prefix.toString(), x, tree, network).toString();

                        if(!count.containsKey(classification))
                            count.put(classification, 0L);

                        count.put(classification, count.get(classification) + 1);
                    }
                }

                JsonObject report = new JsonObject();
                report.addProperty("ROUTES", counter);
                for (Map.Entry<String, Long> x : count.entrySet())
                    report.addProperty(x.getKey(), x.getValue());

                System.out.println("drawRIB('RIB', " + report.toString() + ");");
            }
        }
    }

    public static List<RIB_AFI> getRIB(File file) throws Exception
    {
        System.out.println("Loading RIB ...");

        ArrayList<RIB_AFI> result = new ArrayList<>();
        DataInputStream stream = new DataInputStream(new GZIPInputStream(new FileInputStream(file), 110000000));

        try
        {
            MRTReader reader = new MRTReader(stream);

            int counter = 0;
            while (stream.available() > 0)
            {
                MRTRecord entry = reader.read();

                if(++counter % 100000 == 0)
                    System.out.println("Counter: " + counter);

                if ((entry instanceof RIB_AFI))
                    result.add((RIB_AFI)entry);
            }
        }
        catch (EOFException e)
        {
            System.out.println("[" + new Date().toString() + "] End of stream found!");
            stream.close();
        }

        return result;
    }
}
