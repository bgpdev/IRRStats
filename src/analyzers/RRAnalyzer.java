package analyzers;

import core.Network;
import irr.RoutingInformationBase;
import irr.RoutingRegistry;
import utility.Matcher;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.stream.Stream;

/**
 * The Routing Registry Analyzer analyzes each route object
 * Prior to running the collected resources should already be downloaded.
 */
public class RRAnalyzer
{
    private static ArrayList<RoutingRegistry> registries = new ArrayList<>();
    private static final int STALE_PERIOD = 1;

    /**
     * The main entry point for the Routing Registry Analyzer.
     * @param argv The commandline parameters, currently unused.
     */
    public static void main(String[] argv) throws Exception
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse("2018-09-01");

        /* ------------------------
         * Load each Registry
         * ------------------------*/
        try (Stream<Path> paths = Files.walk(Paths.get("archives/" + format.format(date) + "/irr/po/")))
        {
            paths.filter(Files::isRegularFile).forEach((Path path) ->
            {
                try
                {
                    RoutingRegistry registry = new RoutingRegistry(path.toFile());
                    registries.add(registry);
                    System.out.println("Loaded " + path.toFile().getName() + ": " + registry.routes.size() + " routes found.");
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            });
        }

        /* ------------------------------------------------------
         * Calculate the STALE_PERIOD threshold date.
         * ------------------------------------------------------*/
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -STALE_PERIOD);
        Date threshold = cal.getTime();

        /* ------------------------------------------------------
         * Loop over each archive to get the archives between
         * now and STALE_PERIOD.
         * ------------------------------------------------------*/
        try (Stream<Path> directories = Files.walk(Paths.get("archives/")))
        {
            directories.filter(Files::isDirectory).forEach((Path path) ->
            {
                try
                {
                    String name = path.getFileName().toString();
                    Date i = format.parse(name);

                    if(threshold.before(i))
                    {
                        Network network = new Network(i.toInstant());
                        try (Stream<Path> paths = Files.walk(Paths.get("archives/" + name + "/ribs/")))
                        {
                            paths.filter(Files::isRegularFile).forEach((Path file) ->
                            {
                                try
                                {
                                    System.out.println("[" + new Date().toString() + "] Loading " + file.toAbsolutePath().toString());
                                    RoutingInformationBase rib = new RoutingInformationBase(file.toFile());
                                    System.out.println("[" + new Date().toString() + "] Done!");

                                    for (RoutingRegistry registry : registries)
                                    {
                                        System.out.println("Checking " + registry.name);
                                        int counter = 0;
                                        for (Map.Entry<String, RoutingRegistry.Entry> entry : registry.routes.entrySet())
                                        {
                                            if (counter++ % 5000 == 0)
                                                System.out.println("Percentage: " + ((double) counter / (double) registry.routes.size()));

                                            if (!entry.getValue().classification.equals(Matcher.Classification.NONE))
                                                continue;

                                            entry.getValue().classification = Matcher.match(entry.getValue().po, rib, network);
                                        }
                                    }
                                } catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            });
                        }
                    }
                }
                catch(Exception e)
                {
                    // Do nothing with unparsable archives.
                }
            });
        }

        for(RoutingRegistry registry : registries)
        {
            System.out.println("Registry: " + registry.name);
            System.out.println(registry.getStatistics().toString());
        }
    }
}
