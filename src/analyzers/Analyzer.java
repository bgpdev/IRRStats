package analyzers;

import irr.RoutingInformationBase;
import irr.RoutingRegistry;
import com.google.gson.JsonObject;

public interface Analyzer
{
    JsonObject analyze(RoutingRegistry registry, RoutingInformationBase rib);
}
