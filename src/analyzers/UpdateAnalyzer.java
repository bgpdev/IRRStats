package analyzers;

import bgp.BGPMessage;
import bgp.BGPReader;
import bgp.BGPUpdate;
import bgp.utility.BGPUtility;
import collections.trees.BinaryTree;
import com.google.gson.JsonObject;
import core.Network;
import irr.RoutingRegistry;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.BGP4MP.MESSAGE;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import network.entities.Prefix;
import po.PO;
import utility.Matcher;

import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.nio.channels.Channels;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class UpdateAnalyzer
{
    private static BinaryTree<ArrayList<PO>> tree = new BinaryTree<>();

    /**
     * The main entry point for the RIB analyzer
     * @param argv The commandline parameters, currently unused.
     */
    public static void main(String[] argv) throws Exception
    {
        /* --------------------------------------------------
         * Load each PO file and construct a PrefixTree.
         * --------------------------------------------------*/
        Date date = new SimpleDateFormat("yyyyMMdd").parse("20180901");
        tree = RoutingRegistry.getRegistryTree(date);
        Network network = new Network(Instant.parse("2018-09-01T00:00:00Z"));

        Instant start = Instant.parse("2018-09-01T00:00:00Z");
        Instant time = Instant.parse("2018-09-01T00:00:00Z");
        long interval = 300;

        // Construct the DateFormats that are used to construct the right URL.
        DateTimeFormatter x = DateTimeFormatter.ofPattern("yyyy.MM").withZone(ZoneOffset.UTC);
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyyMMdd.HHmm").withZone(ZoneOffset.UTC);

        int counter = 0;
        HashMap<String, Long> count = new HashMap<>();

        while(true)
        {
            // Construct the URL which points to a MRT formatted file with BGPUpdates.
            String url = "http://data.ris.ripe.net/rrc00/" + x.format(time) + "/updates." + y.format(time) + ".gz";
            System.out.println("Scraping a new URL: " + url);

            /*
             * TODO: This is done to prevent: java.net.SocketException: Connection reset
             */

            File tmp = File.createTempFile("updates", ".tmp");
            FileOutputStream output = new FileOutputStream(tmp);

            while(true)
            {
                try
                {

                    Thread.sleep(100);
                    output.getChannel().transferFrom(Channels.newChannel(new URL(url).openStream()), 0, Long.MAX_VALUE);
                    break;
                } catch (ConnectException e)
                {
                    System.out.println("Connection error: Retrying");
                }
            }

            try
            {
                DataInputStream stream = new DataInputStream(new GZIPInputStream(new FileInputStream(tmp)));
                MRTReader reader = new MRTReader(stream);
                while (stream.available() > 0)
                {
                    MRTRecord entry = reader.read();

                    if (!(entry instanceof MESSAGE))
                        continue;

                    MESSAGE message = (MESSAGE) entry;

                    BGPReader.Settings settings = new BGPReader.Settings();
                    settings.EXTENDED_AS4 = true;
                    settings.MRTMode = false;
                    BGPReader parser = new BGPReader(new DataInputStream(new ByteArrayInputStream(message.message)), settings);
                    BGPMessage bgp = parser.read();

                    if (!(bgp instanceof BGPUpdate))
                        continue;

                    BGPUpdate update = (BGPUpdate) bgp;
                    BGPUtility.normalize(update);

                    for(Prefix prefix : update.nlri)
                    {
                        counter++;

                        String classification = Matcher.match(prefix.toString(), update.pathattributes, tree, network).toString();

                        if (!count.containsKey(classification))
                            count.put(classification, 0L);

                        count.put(classification, count.get(classification) + 1);
                    }
                }
            }
            catch(EOFException e)
            {
                JsonObject report = new JsonObject();
                report.addProperty("ROUTES", counter);
                for (Map.Entry<String, Long> q : count.entrySet())
                    report.addProperty(q.getKey(), q.getValue());

                System.out.println("drawRIB('RIB', " + report.toString() + ");");
            }

            if(time.plus(interval, ChronoUnit.SECONDS).getEpochSecond() - start.getEpochSecond() > (3600 * 24))
            {
                JsonObject report = new JsonObject();
                report.addProperty("ROUTES", counter);
                for (Map.Entry<String, Long> q : count.entrySet())
                    report.addProperty(q.getKey(), q.getValue());

                System.out.println("drawRIB('RIB', " + report.toString() + ");");

                break;
            }

            // Update the calendar with 5 minutes, in order to scrape the next message.
            time = time.plus(interval, ChronoUnit.SECONDS);
            tmp.delete();
        }
    }
}
