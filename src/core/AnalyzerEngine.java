package core;

import analyzers.RIBAnalyzer;
import analyzers.RRAnalyzer;
import com.google.gson.JsonObject;
import irr.RoutingInformationBase;
import irr.RoutingRegistry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AnalyzerEngine
{
    public void analyzeRIB(ArrayList<RoutingRegistry> registries, Network network, Date date) throws Exception
    {
        /* ------------------------
         * Calculate BGP Update statistics
         * ------------------------*/
//        {
//            SimpleDateFormat x = new SimpleDateFormat("yyyy.MM");
//            SimpleDateFormat y = new SimpleDateFormat("yyyyMMdd.HHmm");
//
//            // Construct Update Analyzer.
//            UpdateAnalyzer analyzer = new UpdateAnalyzer();
//
//            // The RRC is collecting data from this time onward.
//            Calendar calendar = Calendar.getInstance();
//            calendar.set(2018, Calendar.APRIL, 18,0,0);
//
//            Calendar i = Calendar.getInstance();
//            i.set(2018, Calendar.APRIL, 17,0,0);
//
//            long counter = 0;
//            long direct_exact_matches = 0;
//            long upstream_exact_matches = 0;
//            long direct_general_matches = 0;
//            long upstream_general_matches = 0;
//            long link_general_matches = 0;
//            long link_exact_matches = 0;
//            long not_in_registry = 0;
//            long mismatches = 0;
//
//            while (i.before(calendar))
//            {
//                // Construct the URL which points to a MRT formatted file with BGPUpdates.
//                String url = "http://data.ris.ripe.net/rrc00/" + x.format(i.getTime()) + "/updates." + y.format(i.getTime()) + ".gz";
//                System.out.println("Scraping a new URL: " + url);
//
//                DataInputStream stream = new DataInputStream(new GZIPInputStream(new URL(url).openStream(), 500000));
//
//                try
//                {
//                    MRTReader reader = new MRTReader(stream);
//                    while (stream.available() > 0)
//                    {
//                        MRTRecord entry = reader.read();
//
//                        if (!(entry instanceof MESSAGE))
//                            continue;
//
//                        MESSAGE record = (MESSAGE) entry;
//
//                        BGPReader.Settings settings = new BGPReader.Settings();
//                        settings.MRTMode = false;
//                        settings.EXTENDED_AS4 = true;
//                        BGPReader parser = new BGPReader(new DataInputStream(new ByteArrayInputStream(record.message)), settings);
//
//                        BGPMessage message;
//                        message = parser.read();
//
//                        if(message instanceof BGPUpdate)
//                        {
//                            JsonObject report = analyzer.analyze((BGPUpdate)message, registries, network);
//                            counter += report.get("routes").getAsLong();
//                            direct_exact_matches += report.get("direct-exact-matches").getAsLong();
//                            upstream_exact_matches += report.get("upstream-exact-matches").getAsLong();
//                            link_exact_matches += report.get("link-exact-matches").getAsLong();
//                            link_general_matches += report.get("link-general-matches").getAsLong();
//                            direct_general_matches += report.get("direct-general-matches").getAsLong();
//                            upstream_general_matches += report.get("upstream-general-matches").getAsLong();
//                            not_in_registry += report.get("not-in-registry").getAsLong();
//                            mismatches += report.get("mismatches").getAsLong();
//                        }
//                    }
//                } catch (EOFException e)
//                {
//                    System.out.println("[" + new Date().toString() + "] End of stream found!");
//                    stream.close();
//                }
//
//                System.out.println("Routes: " + counter);
//                System.out.println("Direct Exact Matches: " + direct_exact_matches);
//                System.out.println("Direct General Matches: " + direct_general_matches);
//                System.out.println("Link Exact Matches: " + link_exact_matches);
//                System.out.println("Link General Matches: " + link_general_matches);
//                System.out.println("Upstream Exact Matches: " + upstream_exact_matches);
//                System.out.println("Upstream General Matches: " + upstream_general_matches);
//                System.out.println("Not in Registry: " + not_in_registry);
//                System.out.println("Mismatches: " + mismatches);
//
//                // The interval on which BGPUpdates are stored in an archive file in minutes.
//                int interval = 5;
//                i.add(Calendar.MINUTE, interval);
//            }
//        }


        /* ------------------------
         * Calculate RIB statistics
         * ------------------------*/

//        SimpleDateFormat x = new SimpleDateFormat("yyyy.MM");
//        SimpleDateFormat y = new SimpleDateFormat("yyyyMMdd.0000");
//
//        String url = "http://data.ris.ripe.net/rrc00/" + x.format(date.getTime()) + "/bview." + y.format(date.getTime()) + ".gz";
//        RoutingInformationBase rib = new RoutingInformationBase("rrc00", url, date);
//
//        RIBAnalyzer a = new RIBAnalyzer();
//        System.out.println(a.analyze(registries, rib, network));
//
//        /* ------------------------
//         * Calculate RR statistics
//         * ------------------------*/
//
//        {
//            long counter = 0;
//            long direct_exact_matches = 0;
//            long upstream_exact_matches = 0;
//            long direct_general_matches = 0;
//            long upstream_general_matches = 0;
//            long link_general_matches = 0;
//            long link_exact_matches = 0;
//            long not_in_rib = 0;
//            long mismatches = 0;
//
//            RRAnalyzer analyzer = new RRAnalyzer();
//            for (RoutingRegistry registry : registries)
//            {
//                System.out.println(registry.name);
//                JsonObject report = analyzer.analyze(registry, rib, network);
//                counter += report.get("routes").getAsLong();
//                direct_exact_matches += report.get("direct-exact-matches").getAsLong();
//                upstream_exact_matches += report.get("upstream-exact-matches").getAsLong();
//                link_exact_matches += report.get("link-exact-matches").getAsLong();
//                link_general_matches += report.get("link-general-matches").getAsLong();
//                direct_general_matches += report.get("direct-general-matches").getAsLong();
//                upstream_general_matches += report.get("upstream-general-matches").getAsLong();
//                not_in_rib += report.get("not-in-rib").getAsLong();
//                mismatches += report.get("mismatches").getAsLong();
//
//                System.out.println(report.toString());
//                System.out.println("Mismatch percentage: " + ((double) report.get("mismatches").getAsLong() / (double) report.get("routes").getAsLong()));
//            }
//
//            System.out.println("Routes: " + counter);
//            System.out.println("Direct Exact Matches: " + direct_exact_matches);
//            System.out.println("Direct General Matches: " + direct_general_matches);
//            System.out.println("Upstream Exact Matches: " + upstream_exact_matches);
//            System.out.println("Upstream General Matches: " + upstream_general_matches);
//            System.out.println("Link Exact Matches: " + link_exact_matches);
//            System.out.println("Link General Matches: " + link_general_matches);
//            System.out.println("Not in RIB: " + not_in_rib);
//            System.out.println("Mismatches: " + mismatches);
//        }
    }
}
