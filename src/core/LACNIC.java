package core;

import po.POReader;

import java.io.*;
import java.nio.channels.FileChannel;
import java.time.LocalDate;

public class LACNIC
{
    public static void main(String[] argv) throws Exception
    {
        File file = new File("LACNIC.po");

        FileOutputStream out = new FileOutputStream(file);
        final FileChannel outChannel = out.getChannel();

        long position = 0;

        for (File path : new File("LACNIC/LACNIC-crawler").listFiles())
        {
            // For each crawler
            if (path.isDirectory())
            {
                System.out.println("Scanning directory: " + path.getName());
                for (File po : new File("LACNIC/LACNIC-crawler/" + path.getName() + "/LACNIC").listFiles())
                {
                    // Skip all files that are empty.
                    if(po.length() == 0)
                        continue;

                    FileInputStream in = new FileInputStream(po);

                    final FileChannel inChannel = in.getChannel();
                    inChannel.transferTo(position, inChannel.size(), outChannel);
                    position += inChannel.size();

                    inChannel.transferTo(0, inChannel.size(), outChannel);
                    inChannel.close();
                    in.close();

                }
            }
        }

        outChannel.close();
        out.flush();
        out.close();
    }
}
