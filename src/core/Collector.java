package core;

import irr.IRRDownloader;
import irr.RPKI;
import irr.RoutingRegistry;
import providers.ASRank;
import rsef.RSEF;
import whois.resolver.arin.ARINDownloader;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * The Collector class, upon execution downloads all the data sources from today.
 * The data sources being downloaded are:
 * - RSEF-extended listings
 * - IRR dbase files
 * - ASRank serial2 links
 * - ARIN WHOIS dump
 * - LACNIC WHOIS dump
 * - LACNIC WHOIS scrape.
 * - RIPE RRC RIBs for this date.
 */
public class Collector implements Runnable
{
    private final static ArrayList<RoutingRegistry> registries = new ArrayList<>();

    public static void main(String[] argv)
    {
        /* ----------------------------------------------------------------------------------------------
         * Start the Collector in a separate Thread. Execute it every 24 hours.
         * ----------------------------------------------------------------------------------------------*/
        if(argv.length == 1 && argv[0].equals("--continuous"))
        {
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(new Collector(), 0, 3600 * 24, SECONDS);
        }
        else
        {
            Collector collector = new Collector();
            collector.run();
        }
    }

    public Collector()
    {
        // RIR Routing Registries
        registries.add(new RoutingRegistry("AFRINIC", new String[]{"ftp://ftp.afrinic.net/pub/dbase/afrinic.db.gz"}));
        registries.add(new RoutingRegistry("ARIN-BULK", new String[]{}));
        registries.add(new RoutingRegistry("ARIN", new String[]{"ftp://ftp.arin.net/pub/rr/arin.db"}));
        registries.add(new RoutingRegistry("RIPE", new String[]{"https://ftp.ripe.net/ripe/dbase/split/ripe.db.as-block.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.as-set.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.aut-num.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.filter-set.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.inet-rtr.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.peering-set.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.route-set.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.route.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.route6.gz", "https://ftp.ripe.net/ripe/dbase/split/ripe.db.rtr-set.gz"}));
        registries.add(new RoutingRegistry("APNIC", new String[]{"ftp://ftp.apnic.net/pub/apnic/whois/apnic.db.route.gz", "ftp://ftp.apnic.net/pub/apnic/whois/apnic.db.route6.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.rtr-set.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.route-set.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.peering-set.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.inet-rtr.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.filter-set.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.aut-num.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.as-set.gz", "https://ftp.apnic.net/apnic/whois/apnic.db.as-block.gz"}));
        registries.add(new RoutingRegistry("LACNIC-RPKI", new String[]{"http://stats.labs.lacnic.net/RPKI/RPKItoJSON/roas-to-rpsl-lacnic-latest.rpsl"}));

        // Other Routing Registries
        registries.add(new RoutingRegistry("LEVEL3", new String[]{"ftp://rr.level3.net/pub/rr/level3.db.gz", "ftp://rr.level3.net/pub/rr/wcgdb.db.gz"}));
        registries.add(new RoutingRegistry("ALTDB", new String[]{"ftp://ftp.altdb.net/pub/altdb/altdb.db.gz"}));
        registries.add(new RoutingRegistry("AOLTW", new String[]{"ftp://ftp.radb.net/radb/dbase/aoltw.db.gz"}));
        registries.add(new RoutingRegistry("BELL", new String[]{"ftp://whois.in.bell.ca/bell.db.gz"}));
        registries.add(new RoutingRegistry("BBOI", new String[]{"ftp://irr.bboi.net/bboi.db.gz"}));
        registries.add(new RoutingRegistry("CANARIE", new String[]{"ftp://whois.canarie.ca/dbase/canarie.db.gz"}));
        registries.add(new RoutingRegistry("EASYNET", new String[]{"ftp://ftp.radb.net/radb/dbase/easynet.db.gz"}));
        registries.add(new RoutingRegistry("HOST", new String[]{"ftp://ftp.radb.net/radb/dbase/host.db.gz"}));
        registries.add(new RoutingRegistry("JPIRR", new String[]{"ftp://ftp.radb.net/radb/dbase/jpirr.db.gz"}));
        registries.add(new RoutingRegistry("RADB", new String[]{"ftp://ftp.radb.net/radb/dbase/radb.db.gz"}));
        registries.add(new RoutingRegistry("NESTEGG", new String[]{"ftp://ftp.nestegg.net/irr/nestegg.db.gz"}));
        registries.add(new RoutingRegistry("NTTCOM", new String[]{"ftp://rr1.ntt.net/nttcomRR/nttcom.db.gz"}));
        registries.add(new RoutingRegistry("OTTIX", new String[]{"ftp://iskra.ottix.net/pub/ottix.db.gz"}));
        registries.add(new RoutingRegistry("PANIX", new String[]{"ftp://ftp.panix.com/pub/rrdb/panix.db.gz"}));
        registries.add(new RoutingRegistry("OPENFACE", new String[]{"ftp://ftp.radb.net/radb/dbase/openface.db.gz"}));
        registries.add(new RoutingRegistry("REACH", new String[]{"ftp://ftp.radb.net/radb/dbase/reach.db.gz"}));
        registries.add(new RoutingRegistry("RGNET", new String[]{"ftp://rg.net/rgnet/RGNET.db.gz"}));
        registries.add(new RoutingRegistry("RISQ", new String[]{"ftp://rr.risq.net/pub/risq.db.gz"}));
        registries.add(new RoutingRegistry("TC", new String[]{"ftp://ftp.radb.net/radb/dbase/tc.db.gz"}));
        registries.add(new RoutingRegistry("GW", new String[]{"ftp://ftp.gw.net/pub/rr/gw.db.gz"}));
    }

    @Override
    public void run()
    {
        try
        {
            Instant date = Instant.now();
            date = date.truncatedTo(ChronoUnit.DAYS);

            System.out.println("[" + new Date().toString() + "] Collecting sources ...");

            /* ----------------------------------------------------------------------------------------------
             * Download the RSEFNetwork archives and store them on disk in RAW and PO format.
             * ----------------------------------------------------------------------------------------------*/
            {
                // Download the Routing Specification Exchange Format Listings from each RIR
                System.out.println("----------------------- RSEF -----------------------");
                RSEF.download(date);

                // Download the ARIN WHOIS Database, convert it to RPSL and convert it to PO (prefix-origin) files.
                System.out.println("----------------------- ARIN BULK -----------------------");
                ARINDownloader.download();

                // Download the RPSL dbase database of each routing registry and convert it to PO (prefix-origin) files.
                System.out.println("----------------------- IRR -----------------------");
                IRRDownloader.download(registries);

                // Download the ASRank serial2 listings.
                System.out.println("----------------------- ASRANK -----------------------");
                ASRank.download(date);

                // Download the ASRank serial2 listings.
                System.out.println("----------------------- RPKI -----------------------");
                RPKI.download(date);
            }

            System.out.println("[" + new Date().toString() + "] Finished!");
            System.out.println("Sleeping for 1 hour...");
            Thread.sleep(1000 * 60 * 60);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
